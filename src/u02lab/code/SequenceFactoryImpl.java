package u02lab.code;

import java.util.Random;

/**
 * Created by gab on 08/03/17.
 */
public class SequenceFactoryImpl implements SequenceFactory {
    @Override
    public Sequence createRandomBinarySequence(final int sequenceLength) {
        return new SequenceImpl(1, sequenceLength, e -> new Random().nextInt(2));
    }

    @Override
    public Sequence createRangeSequence(final int start, final int stop) {
        return new SequenceImpl(start, stop, e -> e);
    }
}
