package u02lab.code;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

/**
 * Created by gab on 08/03/17.
 *
 * The implementation of Sequence Interface, based on the strategy pattern.
 */
public class SequenceImpl implements Sequence {

    private final Function<Integer, Integer> strategy;
    private final int start;
    private final int stop;
    private int current;

    public SequenceImpl(final int start, final int stop, final Function<Integer, Integer> strategy) {
        this.start = start;
        this.stop = stop;
        this.current = start;
        this.strategy = strategy;
    }

    @Override
    public Optional<Integer> next() {
        if (!this.isOver()) {
            return Optional.of(this.strategy.apply(current++));
        } else {
            return Optional.empty();
        }
    }

    @Override
    public void reset() {
        this.current = this.start;
    }

    @Override
    public boolean isOver() {
        return this.current > this.stop;
    }

    @Override
    public List<Integer> allRemaining() {
        final List<Integer> allRemaining = new ArrayList<>();
        while (!this.isOver()) {
            allRemaining.add(this.next().get().intValue());
        }
        return allRemaining;
    }
}
