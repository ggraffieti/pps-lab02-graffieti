package u02lab.code;

/**
 * A factory for a sequence of integer numbers, based on a strategy for extracting them.
 */
public interface SequenceFactory {
    /**
     * Creates a sequence of random binary numbers, of length sequenceLength
     * @param sequenceLength the length of the sequence
     * @return a SequenceImpl of random binary numbers.
     */
    Sequence createRandomBinarySequence(int sequenceLength);

    /**
     * Creates a sequence of integer numbers, from start (included) to stop (included).
     * @param start the lower bound of the sequence
     * @param stop the upper bound of the sequence
     * @return a sequence of integer numbers, from start to stop.
     */
    Sequence createRangeSequence(int start, int stop);
}
