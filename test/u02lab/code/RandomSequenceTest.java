package u02lab.code;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by gab on 08/03/17.
 */
public class RandomSequenceTest {

    public static final int SEQUENCE_SIZE = 10;

    private Sequence randomSequence;

    @Before
    public void setup() {
        randomSequence = new SequenceFactoryImpl().createRandomBinarySequence(SEQUENCE_SIZE);
    }

    @Test
    public void isNotOverAfterCreation() {
        assertFalse(randomSequence.isOver());
    }

    @Test
    public void testIsOver() {
        for (int i = 0; i < SEQUENCE_SIZE; i++) {
            assertFalse(randomSequence.isOver());
            randomSequence.next();
        }
        assertTrue(randomSequence.isOver());
    }

    @Test
    public void testValueIsPresent() {
        for (int i = 0; i < SEQUENCE_SIZE; i++) {
            assertTrue(randomSequence.next().isPresent());
        }
    }

    @Test
    public void testOnlyBinaryDigits() {
        for (int i = 0; i < SEQUENCE_SIZE; i++) {
            final int randomValue = randomSequence.next().get().intValue();
            assertTrue(randomValue == 1 || randomValue == 0);
        }
    }

    @Test
    public void testResetAfterOver() {
        iterate(SEQUENCE_SIZE);
        assertTrue(randomSequence.isOver());
        randomSequence.reset();
        assertFalse(randomSequence.isOver());
    }

    @Test
    public void testReset() {
        this.iterate(SEQUENCE_SIZE / 2);
        randomSequence.reset();
        for (int i = 0; i < SEQUENCE_SIZE; i++) {
            assertFalse(randomSequence.isOver());
            assertTrue(randomSequence.next().isPresent());
        }
    }

    @Test
    public void testAllRemainingListAfterCreation() {
        assertEquals(randomSequence.allRemaining().size(), SEQUENCE_SIZE);
    }

    private void iterate(int numberOfIterations) {
        for (int i = 0; i < numberOfIterations; i++) {
            randomSequence.next();
        }
    }

}