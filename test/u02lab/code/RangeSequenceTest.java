package u02lab.code;

import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

/**
 * Created by gab on 08/03/17.
 */
public class RangeSequenceTest {

    private static final int START = 1;
    private static final int STOP = 10;

    private Sequence rangeSequence;

    @Before
    public void setup() {
        rangeSequence = new SequenceFactoryImpl().createRangeSequence(START, STOP);
    }

    @Test
    public void isNotOverAfterCreation() {
        assertFalse(rangeSequence.isOver());
    }

    @Test
    public void existNextAfterCreation() {
        assertTrue(rangeSequence.next().isPresent());
    }

    @Test
    public void testIsOver() {
        for (int i = START; i <= STOP; i++) {
            assertFalse(rangeSequence.isOver());
            rangeSequence.next();
        }
        assertTrue(rangeSequence.isOver());
    }

    @Test
    public void emptyOptionalAfterOver() {
        this.iterate(STOP);
        assertFalse(rangeSequence.next().isPresent());
    }

    @Test
    public void rightNumberReturnedByNext() {
        for (int i = START; i <= STOP; i++) {
            final Optional<Integer> next = rangeSequence.next();
            assertTrue(next.isPresent());
            assertEquals(i, next.get().intValue());
        }
    }

    @Test
    public void testInitialValueReturnedAfterReset() {
        this.iterate(STOP / 2);
        rangeSequence.reset();
        final Optional<Integer> next = rangeSequence.next();
        assertTrue(next.isPresent());
        assertEquals(next.get().intValue(), START);
    }

    @Test
    public void rangeGeneratorIsOverAfterAllRemaining() {
        rangeSequence.allRemaining();
        assertTrue(rangeSequence.isOver());
    }

    @Test
    public void emptyListReturnedIfRangeGeneratorIsOver() {
        iterate(STOP);
        final List<Integer> list = rangeSequence.allRemaining();
        assertTrue(list.isEmpty());
    }

    @Test
    public void correctAllRemainingReturned() {
        final List<Integer> list = rangeSequence.allRemaining();
        for (int i = START; i <= STOP; i++) {
            assertEquals(i, list.get(i - 1).intValue());
        }
    }

    private void iterate(final int max) {
        for (int i = START; i <= max; i++) {
            rangeSequence.next();
        }
    }

}